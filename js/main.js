;(function(){
    let altura = ($(window).height() - $("#description").height())
    console.log(altura)
    window.onscroll = ()=>{
        if(document.body.scrollTop >= (altura)){
            $("#description").addClass("cambio");
        }else{
            $("#description").removeClass("cambio")  ;
        }
    }

    let currentPosition = 0 
    const imageCounter = $(".image").length - 1
    console.log(imageCounter)
    setInterval(()=>{
        if(currentPosition < imageCounter){
            currentPosition++
        }else{
            currentPosition = 0
        }
        $("#galeria .inner").css({
            left: "-"+ currentPosition * 100 +"%"
        })
    }, 3000)
})()